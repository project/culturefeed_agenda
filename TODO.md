## Dependencies

### To be fixed:
* SearchV3
  * Upgrade path to be prepared
* culturefeed_search
    * To be removed
* Purl
  * Should be replaced

### OK: 
* CalendarSummaryV3
    * Should be compatible
* culturefeed_search_api
    * Should be compatible
* Intl php extension should be enabled